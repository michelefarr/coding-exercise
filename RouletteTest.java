package src.test.java.programming.exercise;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import src.main.java.programming.exercise.Player;
import src.main.java.programming.exercise.Roulette;

public class RouletteTest {
	
    private Roulette roulette;
    private Player player1;
	private List<Player> playersList;
	
    /**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
    	roulette = new Roulette();
    	player1 = new Player();
    	player1.setName("Robert");
    	player1.setAmountBet(10.0);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRoulette1() {
		//Check that the player win if his bet is equals to the extracted number
		player1.setBet("10");
    	playersList = new ArrayList<Player>();
    	playersList.add(player1);
    	roulette.checkWinners(playersList, 10);
		Assert.assertEquals(true, player1.isHasWon());
	}
	
	@Test
	public void testRoulette2() {
		//Check that the player win if his bet is "EVEN" and the extracted number is even
		player1.setBet("EVEN");
    	playersList = new ArrayList<Player>();
    	playersList.add(player1);
    	roulette.checkWinners(playersList, 10);
		Assert.assertEquals(true, player1.isHasWon());	
	}
	
	@Test
	public void testRoulette3() {
		//Check that the player win if his bet is "ODD" and the extracted number is odd
		player1.setBet("ODD");
    	playersList = new ArrayList<Player>();
    	playersList.add(player1);
    	roulette.checkWinners(playersList, 5);
		Assert.assertEquals(true, player1.isHasWon());	
	}	
	
	@Test
	public void testRoulette4() {
		//Check that the player lose if his bet is not equals to the extracted number
		player1.setBet("1");
    	playersList = new ArrayList<Player>();
    	playersList.add(player1);
    	roulette.checkWinners(playersList, 10);
		Assert.assertEquals(false, player1.isHasWon());
	}
	
	@Test
	public void testRoulette5() {
		//Check if for 1 bet the total bet amount is equals to the betAmount
		player1.setBet("30");
    	playersList = new ArrayList<Player>();
    	playersList.add(player1);
    	roulette.checkWinners(playersList, 30);
		Assert.assertEquals(new Double(10.0), new Double(player1.getTotalBet()));	
	}
	
	@Test
	public void testRoulette6() {
		//If the beth is a number, the player win 36*betAmount
		player1.setBet("30");
    	playersList = new ArrayList<Player>();
    	playersList.add(player1);
    	roulette.checkWinners(playersList, 30);
		Assert.assertEquals(new Double(360.0), new Double(player1.getTotalWon()));	
	}
	
	@Test
	//If the beth is "EVEN", the player win 2*betAmount
	public void testRoulette7() {
		player1.setBet("EVEN");
    	playersList = new ArrayList<Player>();
    	playersList.add(player1);
    	roulette.checkWinners(playersList, 10);
		Assert.assertEquals(new Double(20.0), new Double(player1.getTotalWon()));	
	}
	
	@Test 
	//If the beth is "ODD", the player win 2*betAmount
	public void testRoulette8() {
		player1.setBet("ODD");
    	playersList = new ArrayList<Player>();
    	playersList.add(player1);
    	roulette.checkWinners(playersList, 5);
		Assert.assertEquals(new Double(20.0), new Double(player1.getTotalWon()));	
	}
}
