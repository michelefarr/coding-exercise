package src.main.java.programming.exercise;

public class Player {
	private String name;
	private String bet;
	private double amountBet;
	private double amountWon;
	private double totalBet;
	private double totalWon;
	private boolean hasWon;
	
	public double getAmountWon() {
		return amountWon;
	}
	public void setAmountWon(double amountWon) {
		this.amountWon = amountWon;
	}
	public void setAmountBet(double amountBet) {
		this.amountBet = amountBet;
	}
	public double getAmountBet() {
		return amountBet;
	}
	public String getBet() {
		return bet;
	}
	public void setBet(String bet) {
		this.bet = bet;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getTotalBet() {
		return totalBet;
	}
	public void setTotalBet(double totalBet) {
		this.totalBet = totalBet;
	}
	public double getTotalWon() {
		return totalWon;
	}
	public void setTotalWon(double totalWon) {
		this.totalWon = totalWon;
	}
	public boolean isHasWon() {
		return hasWon;
	}
	public void setHasWon(boolean hasWon) {
		this.hasWon = hasWon;
	}
}
