package src.main.java.programming.exercise;

import java.util.Random;

public class SpinTheWeel implements Runnable {
	Random random = new Random();
	Thread thread;
	
	public SpinTheWeel() {
		thread = new Thread(this);
		thread.setPriority(Thread.MAX_PRIORITY);
		thread.start();
	}

	public void run() {
		try{
			Thread.sleep(30000);
		}
		catch(InterruptedException ie) {
			ie.printStackTrace();
		}
	}
	
	public synchronized int getExtractedNumber() {
		return random.nextInt(36);
	}
}
