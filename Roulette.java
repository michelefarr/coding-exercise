package src.main.java.programming.exercise;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Roulette {
	
	public List<Player> checkWinners(List<Player> playersList, int extractedNumber) {
		Player player;
		Iterator<Player> iterator = playersList.iterator();
		
		System.out.print("\n\nNumber extracted: " + extractedNumber);
		System.out.print("\nPlayer\tBet\tOutcome\tWinnings\tTotal Won\tTotal Bet");
		System.out.print("\n------\t---\t-------\t--------\t---------\t---------");
		
		while(iterator.hasNext()) {
			double amountWon = 0.0;
			player = iterator.next();
			if(player.getBet().equals(String.valueOf(extractedNumber))) {
				amountWon = Double.valueOf(player.getAmountBet()*36);
				player.setAmountWon(amountWon);
				player.setTotalWon(player.getTotalWon() + amountWon);
				player.setTotalBet(player.getTotalBet() + player.getAmountBet());
				player.setHasWon(true);
			}
			else if(player.getBet().equals("EVEN") && extractedNumber%2==0) {
				amountWon = Double.valueOf(player.getAmountBet()*2);
				player.setAmountWon(amountWon);
				player.setTotalWon(player.getTotalWon() + amountWon);
				player.setTotalBet(player.getTotalBet() + player.getAmountBet());
				player.setHasWon(true);			
			}
			else if(player.getBet().equals("ODD") && extractedNumber%2>0) {
				amountWon = Double.valueOf(player.getAmountBet()*2);
				player.setAmountWon(amountWon);
				player.setTotalWon(player.getTotalWon() + amountWon);
				player.setTotalBet(player.getTotalBet() + player.getAmountBet());
				player.setHasWon(true);			
			}
			else {
				player.setAmountWon(0.0);
				player.setTotalBet(player.getTotalBet() + player.getAmountBet());
				player.setHasWon(false);	
			}
			System.out.print("\n" + player.getName() + "\t" + player.getBet() + "\t" +(player.isHasWon()?"WIN\t":"LOSE\t") + player.getAmountWon() + "\t\t" + player.getTotalWon() + "\t\t" + player.getTotalBet());
		}
		return playersList;
	}
	
    public static void main(String[] args) {
		String line;
		BufferedReader in = null;
		Roulette roulette;
		Player player;
		List<Player> playerList;
		SpinTheWeel spinTheWeel;
		int extractedNumber = 0;
		
		try {
			//open the input file
			in = new BufferedReader(new FileReader(args[0]));
			
			roulette = new Roulette();
			playerList = new ArrayList<Player>();
			
			//read the input file
			while ((line = in.readLine()) != null) {
				String[] lineArray = line.split(" ");
				player = new Player();
				player.setName(lineArray[0]);
				player.setBet(lineArray[1]);
				player.setAmountBet(Double.parseDouble(lineArray[2]));
				
				playerList.add(player);
			}
			
			spinTheWeel = new SpinTheWeel();
			while(true) {
				extractedNumber = spinTheWeel.getExtractedNumber();
				playerList = roulette.checkWinners(playerList, extractedNumber);
				spinTheWeel.run();
			}
		}
		catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				in.close();
			}
			catch(IOException ioe){
				ioe.printStackTrace();
			}
		}
    }
}